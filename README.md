# Evaluation Scripts for OLIC Protocol

This repo contains the source code for evaluation of OLIC protocol from the
paper _“Where are you Bob? Privacy-Preserving Proximity Testing with a Napping
Party”_ by Ivan Oleynikov, Elena Pagnin and Andrei Sabelfeld.

## Dependencies

  * <https://bitbucket.org/hallgrep/bettertimes/> must be installed for Python2 (using `pip2`)
  * <https://github.com/lc6chang/ecc-pycrypto> must be installed for Python3
    (using `pip3`). Additionally, we require to apply the provided patches to
    `ecc-pycrypto` at commit `eeeab3ab24a9843f6b86f8b2a98df228319983d2`. More
    specifically, you should do:
   
```bash
# Assuming you start in the root of this repo
git clone https://github.com/lc6chang/ecc-pycrypto
git -C ecc-pycrypto reset --hard eeeab3ab24a9843f6b86f8b2a98df228319983d2
git -C ecc-pycrypto/ am $(realpath patches)/*

# Now, you can install ./ecc-pycrypto
cd ecc-pycrypto
python3 ./setup.py install
```

## Usage

All the scripts print the data in the CSV table format.

 * Run `./communication.py` to get estimated communication cost.
 * Run `./ellipticElGamal.py` to get running time of all the parties
   for (EC) version—which uses Elliptic Curve ElGamal with curves Curve25519 and
   M383.
 * Run `./numericElGamal.py` to get running time of all the parties
   for (non-EC) version—which uses ElGamal and Paillier over `Z_p` field.
