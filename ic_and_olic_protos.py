# -*- coding: utf-8 -*-

# The bettertimes library can be found here:
# <https://bitbucket.org/hallgrep/bettertimes/src/208b110e2b2007c2040acb110d1e2d7529ebaa9f>.
# We use its cryptographic operations because it was used in IC work

from gmpy2 import powmod
from gmpy2 import random_state, mpz_random, gcd
from time import time

from random import shuffle

rs = random_state(int(time()))

def sampleInvertible(q):
  candidate = mpz_random(rs, q - 1) + 1
  if gcd(candidate, q) == 1:
    return candidate
  else:
    return sampleInvertible(q)

# --- Core Functions of OLIC protocol ----

def aStart(s, pk, xa, ya):
  a1 = s.raw_encrypt(pk, xa ** 2 + ya ** 2)
  a2 = s.raw_encrypt(pk, 2 * xa)
  a3 = s.raw_encrypt(pk, 2 * ya)

  return (a1, a2, a3)

# We precompute the list of such numbers which can be represented as a sum of
# two squares. We use it in lessThan so that we don't have to go through all the
# numbers.
sumsOfSquares = []
for i in range(10001):
  found = False
  a = 0
  while (not found) and a**2 <= i:
    b = 0
    while (not found) and a**2 + b**2 <= i:
      if a**2 + b**2 == i:
        found = True
      b += 1
    a += 1
  if found:
    sumsOfSquares.append(i)


def lessThan(s, pk, cache, d, rsq):
  q = s.plaintext_space(pk)
  l = []
  for i in [x for x in sumsOfSquares if x < rsq]:
    x = s.sub_const(pk, cache, d, i)
    t = mpz_random(rs, q - 1) + 1
    h = s.mul_const(pk, x, t)
    l.append(h)
    shuffle(l)
  return l

def checkLessThan(s, sk, l):
  q = s.plaintext_space(sk)
  for h in l:
    v = s.raw_decrypt(sk, h)
    if v % q == 0:
      return True
  return False

def bStart(s_, q, pk1, pk2, xb, yb):
  r1 = mpz_random(rs, q)
  r2 = sampleInvertible(q)
  r3 = sampleInvertible(q)

  b1 = (xb**2 + yb**2 + r1) % q
  b2 = (xb * r2) % q
  b3 = (yb * r3) % q

  b1_ = (q - r1) % q
  b2_ = powmod(r2, -1, q)
  b3_ = powmod(r3, -1, q)

  assert (b1 + b1_) % q == (xb**2 + yb**2) % q
  assert (b2 * b2_) % q == xb
  assert (b3 * b3_) % q == yb

  b1 = s_.raw_encrypt(pk1, b1)
  b2 = s_.raw_encrypt(pk1, b2)
  b3 = s_.raw_encrypt(pk1, b3)

  b1_ = s_.raw_encrypt(pk2, b1_)
  b2_ = s_.raw_encrypt(pk2, b2_)
  b3_ = s_.raw_encrypt(pk2, b3_)

  return (b1, b2, b3, b1_, b2_, b3_)

def compTerms(s, s_, sk, sk1, pk, a1, a2, a3, b1, b2, b3):
  t1 = s_.raw_decrypt(sk1, b1)
  t2 = s_.raw_decrypt(sk1, b2)
  t3 = s_.raw_decrypt(sk1, b3)

  c1 = s.add(pk, a1, s.raw_encrypt(pk, t1))
  c2 = s.mul_const(pk, a2, t2)
  c3 = s.mul_const(pk, a3, t3)

  return (c1, c2, c3)

def unblind(s, s_, sk, sk2, pk, c1, c2, c3, b1_, b2_, b3_):
  t1 = s_.raw_decrypt(sk2, b1_)
  t2 = s_.raw_decrypt(sk2, b2_)
  t3 = s_.raw_decrypt(sk2, b3_)

  d1 = s.add(pk, c1, s.raw_encrypt(pk, t1))
  d2 = s.mul_const(pk, c2, t2)
  d3 = s.mul_const(pk, c3, t3)

  # D = s.add(pk, d1, s.add(pk, d2, d3))
  D = s.sub(pk, s.sub(pk, d1, d2), d3)

  return D

# ---- IC and Our Prococol Implementation ----

# The actual function of Alice and Bob's inputs we want to compute
def result(xa, ya, xb, yb, rsq):
  return (xa - xb)**2 + (ya - yb)**2 < rsq

# The InnerCircle protocol. The parameters are:
#  * s — the LHE cryptosystem to use
#  * rsq — raduis squared (agreed beforehand in the real use)
#  * xa, ya — Alice's inputs
#  * xb, yb — Bob's inputs
def ic(s, rsq, ks, xa, ya, xb, yb):
  (sk, pk) = ks
  q = s.plaintext_space(pk)

  cache = s.generate_cache(pk, rsq)

  # The next function is the actions Bob does before running lessThan. In the
  # paper, we didn't give it any name, but it's shown on a picture of the IC
  # protocol.
  def bob(pk, a1, a2, a3):
    b1 = s.add(pk, a1, s.raw_encrypt(pk, (xb**2 + yb**2) % q))
    b2 = s.mul_const(pk, a2, xb)
    b3 = s.mul_const(pk, a3, yb)
    d = s.sub(pk, s.sub(pk, b1, b2), b3)
    return d

  atime = 0
  btime = 0

  t = time()
  (a1, a2, a3) = aStart(s, pk, xa, ya)
  atime += time() - t

  t = time()
  d = bob(pk, a1, a2, a3)

  L = lessThan(s, pk, cache, d, rsq)
  btime += time() - t

  t = time()
  res = checkLessThan(s, sk, L)
  atime += time() - t

  assert res == result(xa, ya, xb, yb, rsq)
  return (atime, btime)

def olic(s, s_, rsq, ks, ks1, ks2, xa, ya, xb, yb):
  (sk, pk) = ks
  (sk1, pk1) = ks1
  (sk2, pk2) = ks2
  q = s.plaintext_space(pk) # In the real protocol this q should be agreed on before the protocol execution, not generated here

  cache = s.generate_cache(pk, rsq)

  # Now, parties execute the protocol. These steps correspond 1-to-1 to the
  # picture from the paper. We also measure time it took to execute each step.

  t = time()
  (a1, a2, a3) = aStart(s, pk, xa, ya)
  aStartTime = time() - t

  t = time()
  (b1, b2, b3, b1_, b2_, b3_) = bStart(s_, q, pk1, pk2, xb, yb)
  bStartTime = time() - t

  t = time()
  (c1, c2, c3) = compTerms(s, s_, sk, sk1, pk, a1, a2, a3, b1, b2, b3)
  compTermsTime = time() - t

  t = time()
  d = unblind(s, s_, sk, sk2, pk, c1, c2, c3, b1_, b2_, b3_)
  unblindTime = time() - t

  t = time()
  L = lessThan(s, pk, cache, d, rsq)
  lessThanTime = time() - t

  t = time()
  res = checkLessThan(s, sk, L)
  checkLessThanTime = time() - t

  assert res == result(xa, ya, xb, yb, rsq)
  return ( aStartTime + checkLessThanTime
         , bStartTime
         , compTermsTime + unblindTime + lessThanTime
         )
