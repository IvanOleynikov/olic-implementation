#!/usr/bin/env python3

from dataclasses import dataclass

from ecc.curve import Curve, Point, P256, secp256k1, Curve25519, M383

from ic_and_olic_protos import *
from gmpy2 import mpz

@dataclass
class EccElGamal():
    curve : Curve

    def keygen(self):
        n = self.curve.n
        sk = mpz_random(rs, n - 1) + 1
        G = Point(self.curve.gx, self.curve.gy, curve=self.curve)
        pk = sk * G
        return (sk, pk)

    def raw_encrypt(self, pk, m):
        n = self.curve.n
        k = mpz_random(rs, n - 1) + 1
        G = Point(self.curve.gx, self.curve.gy, curve=self.curve)
        c1 = k * G
        c2 = m * G + k * pk
        return (c1, c2)

    def raw_decrypt(self, sk, x):
        (c1, c2) = x
        n = self.curve.n
        G = Point(self.curve.gx, self.curve.gy, curve=self.curve)
        m = c2 + (n - sk) * c1
        # print(m, n * G)
        if m is None:
            return 0
        else:
            return 1

    def plaintext_space(self, pk):
        return self.curve.n

    def mul_const(self, pk, x, t):
        (c1, c2) = x
        return (t * c1, t * c2)

    def add(self, pk, x, x_):
        (c1, c2)   = x
        (c1_, c2_) = x_
        return (c1 + c1_, c2 + c2_)

    def sub(self, pk, x, x_):
        (c1, c2)   = x
        (c1_, c2_) = x_
        return (c1 - c1_, c2 - c2_)

    def generate_cache(self, pk, rsq):
        n = self.curve.n
        G = Point(self.curve.gx, self.curve.gy, curve=self.curve)
        cache = {}
        for i in [x for x in sumsOfSquares if x < rsq]:
          cache[-i] = (n - i) * G
        return cache

    # Adds a constant to a encrypted value. Faster than encrypt + add
    def add_const(self, pk, cache, x, i):
        (c1, c2) = x
        return (c1, c2 + cache[i])

    def sub_const(self, pk, cache, x, i):
        return self.add_const(pk, cache, x, -i)

class EccElGamalSimple(EccElGamal):
    def raw_encrypt(self, pk, m):
        n = self.curve.n
        k = mpz_random(rs, n - 1) + 1
        G = Point(self.curve.gx, self.curve.gy, curve=self.curve)
        c1 = k * G
        c2 = self.curve.encode_point(int(m).to_bytes(33, 'big')) + k * pk
        return (c1, c2)

    def raw_decrypt(self, sk, x):
        (c1, c2) = x
        n = self.curve.n
        G = Point(self.curve.gx, self.curve.gy, curve=self.curve)
        m = c2 + (n - sk) * c1
        m.x = int(m.x)
        m.y = int(m.y)
        return mpz(int.from_bytes(self.curve.decode_point(m), 'big'))

if __name__ == "__main__":
  xa = 1
  ya = 1
  xb = 4
  yb = 4

  its = 10

  # Everyone generate the keys. We do this here once, since we do not want to
  # count the time spent generating them.
  s = EccElGamal(Curve25519)
  s_ = EccElGamalSimple(M383)
  ks = s.keygen()
  ks1 = s_.keygen()
  ks2 = s_.keygen()

  print("r,icAlice,icBob,olicAlice,olicBob,olicServers")
  for r in range(0,101,5):
    rsq = r**2
    (icAlice, icBob, olicAlice, olicBob, olicServers) = (0, 0, 0, 0, 0)
    for unused in range(0, its):
      (olicAlice_, olicBob_, olicServers_) = olic( s
                                              , s_
                                              , rsq
                                              , ks, ks1, ks2
                                              , xa, ya
                                              , xb, yb
                                              )
      (icAlice_, icBob_) = ic(s, rsq, ks, xa, ya, xb, yb)
      (icAlice, icBob, olicAlice, olicBob, olicServers) = \
          (icAlice+icAlice_, icBob+icBob_, olicAlice+olicAlice_, olicBob+olicBob_, olicServers+olicServers_)
    (icAlice, icBob, olicAlice, olicBob, olicServers) = (icAlice/its, icBob/its, olicAlice/its, olicBob/its, olicServers/its)
    print("%d,%.4f,%.4f,%.4f,%.4f,%.4f" % (r, icAlice, icBob, olicAlice, olicBob, olicServers))
