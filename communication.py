#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from ic_and_olic_protos import sumsOfSquares

print "r,numericMem,ellipticMem"
for i in xrange(0, 101, 10):
  l = len([x for x in sumsOfSquares if x < i**2])
  print "%d,%.3f,%.3f" % (i, 2 * 2048 / 8 * (12 + l) / 10.0**6, 4 * 390 / 8 * (12 + l) / 10.6**6)
