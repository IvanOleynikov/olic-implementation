#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from bettertimes.crypto.keypair import KeyPair
from bettertimes.crypto.schemes.elgamal import ElGamal
from bettertimes.crypto.schemes.paillier import Paillier

from bettertimes.crypto.schemes.base_scheme import BaseScheme

import gmpy2
from bettertimes.crypto.keypair import KeyPair
from bettertimes.crypto.schemes.elgamal import PrivateKey, PublicKey

from gmpy2 import powmod, divm, mpz_random

from ic_and_olic_protos import *

lam = 2048 # key length for LHE scheme
lam_ = 2148 # key length for the encryption between Bob and the servers

## The original ElGamal class from Bettertimes is a bit buggy -- its method for
# getting plaintext space size actually returns the modulus of ciphertext space,
# which is greater by one. In this MyElGamal class we fix this issue.
#
# Also, we slightly change the key generation algorithm to ensure that q is a
# safe prime (without it it's hard to prove that the group <g> is big enough).
# 
class MyElGamal(ElGamal):
    def __init__(self):
      super(MyElGamal, self).__init__()

    def keygen(self, bits):
        # The next "while True" takes considerable amount of time to run — up to
        # 10-15 min on 4.6 GHz CPU. If you want to save time during debugging,
        # try replacing the whole loop with the following hardcoded value for q:
        #
        # q = 22845058592451581967572453020007335116622949419670554058294106467170533408070696388415924681442893819128057324722223547480402453121401724521936985388226575774721494175077636412427163953932290595716126350835752959339966066863493622211497726595551860244813645732811628603381007233742702260342884098507197777923865569502416585570526945280617718241351021847746421467847875922574635151718955709330167349627753856206525342737008524781257350749756479009513643159624918026975917926808475692684521044297926516801572059917334322141723999591271764045952217262087572325360586601283925026426392850406128765123700359247551606468363

        while True:
          # We use Pocklington criterion here:
          # <https://en.wikipedia.org/wiki/Pocklington_primality_test#Pocklington_criterion>
          seed = gmpy2.mpz_urandomb(rs, bits)
          p = gmpy2.next_prime(seed)
          q = 2*p + 1
          a = gmpy2.mpz_random(rs, q - 2) + 2
          # print p
          if gmpy2.powmod(a, q - 1, q) != 1:
            # print "failed #1"
            continue
          if gmpy2.gcd(a * a - 1, q) != 1:
            # print "failed #2"
            continue
          break

        print(q)

        g = gmpy2.mpz_random(rs, q)

        # Secret
        x = gmpy2.mpz_random(rs, q)

        # Also public
        h = gmpy2.powmod(g, x, q)

        # Done
        public_key = PublicKey(q, g, h, bits)
        return KeyPair(public_key, PrivateKey(x))

    def raw_decrypt(self, private_key, ciphertext):
        c1, c2 = ciphertext

        # We can calculate s using the private key, and use it to cancel out y
        s = gmpy2.powmod(c1, private_key.x, private_key.q)

        # This is the actual decryption
        mapped_element = gmpy2.divm(c2, s, private_key.q)

        # Map the group element to our concrete plaintext space
        if mapped_element == 1 or mapped_element == gmpy2.powmod(private_key.g, self.plaintext_space(private_key), private_key.q):
          return 0
        else:
          return 1

    @staticmethod
    def plaintext_space(public_key):
        return (public_key.q - 1) // 2

    def generate_cache(self, pk, rsq):
      q = self.cipher_space(pk)
      g = pk.g
      cache = {}
      for i in [x for x in sumsOfSquares if x < rsq]:
        cache[-i] = powmod(g, -i, q)
      return cache

    # Adds a constant to a encrypted value. Faster than encrypt + add
    def add_const(self, pk, cache, x, c):
      q = self.cipher_space(pk)
      g = pk.g
      (c1, c2) = x
      c_ = cache.get(c)
      assert c_ is not None
      return (c1, (c2 * c_) % q)

    def sub_const(self, pk, cache, x, c):
      return self.add_const(pk, cache, x, -c)

## Identity encryption for testing purposes
#
class IdEnc(BaseScheme):
  def keygen(self, n):
    return KeyPair(None, None)
  def raw_encrypt(self, k, x):
    return x
  def raw_decrypt(self, k, x):
    return x

if __name__ == "__main__":
  xa = 1
  ya = 1
  xb = 4
  yb = 4

  its = 10

  # Alice generates the keys. We do this here once, since generating of safe
  # primes is time-consuming.
  s = MyElGamal()
  s_ = Paillier()
  sk = s.keygen(lam)
  pk = sk.public_key
  sk1 = s_.keygen(lam_)
  pk1 = sk1.public_key
  sk2 = s_.keygen(lam_)
  pk2 = sk2.public_key

  print "r,icAlice,icBob,olicAlice,olicBob,olicServers"
  for r in xrange(0,101,5):
    rsq = r**2
    (icAlice, icBob, olicAlice, olicBob, olicServers) = (0, 0, 0, 0, 0)
    for unused in range(0, its):
      (olicAlice_, olicBob_, olicServers_) = olic( s
                                              , s_
                                              , rsq
                                              , (sk, pk)
                                              , (sk1, pk1)
                                              , (sk2, pk2)
                                              , xa, ya
                                              , xb, yb
                                              )
      (icAlice_, icBob_) = ic(s, rsq, (sk, pk), xa, ya, xb, yb)
      (icAlice, icBob, olicAlice, olicBob, olicServers) = \
        (icAlice+icAlice_, icBob+icBob_, olicAlice+olicAlice_, olicBob+olicBob_, olicServers+olicServers_)
    (icAlice, icBob, olicAlice, olicBob, olicServers) = (icAlice/its, icBob/its, olicAlice/its, olicBob/its, olicServers/its)
    print "%d,%.4f,%.4f,%.4f,%.4f,%.4f" % (r, icAlice, icBob, olicAlice, olicBob, olicServers)
