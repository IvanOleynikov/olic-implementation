#!/bin/sh -xefu

rm -rf ecc-pycrypto
rm -rf source-code.zip

# Include the ecc-pycrypto for backup purpose
git clone https://github.com/lc6chang/ecc-pycrypto
git -C ecc-pycrypto reset --hard eeeab3ab24a9843f6b86f8b2a98df228319983d2

files="communication.py ellipticElGamal.py ic_and_olic_protos.py make-archive.sh numericElGamal.py README.md patches/0001-Add-quick-and-dirty-support-for-point-at-infinity.patch patches/0002-Replace-Python-arithmetics-by-gmpy2-library-in-an-at.patch"

zip source-code.zip $files $(find .git/ -type f) $(find ecc-pycrypto/ -type f)
